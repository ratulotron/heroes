import { Heroes2Page } from './app.po';

describe('heroes2 App', () => {
  let page: Heroes2Page;

  beforeEach(() => {
    page = new Heroes2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
